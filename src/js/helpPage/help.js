/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2022 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

/* eslint-env jquery */
/* eslint-disable func-names */

function accordionCollapseScroll() {
  // eslint-disable-next-line func-names
  $('.collapse').on('shown.bs.collapse', function (e) {
    let $card;
    if (e.target.className.includes('collapseCollect')) {
      $card = $(this).closest('.collapseCollectButton');
    } else {
      $card = $(this).closest('.accordion-item');
    }
    $('html,body').animate(
      {
        scrollTop: $card.offset().top,
      },
      500,
    );
    e.stopPropagation();
  });

  try {
    const manifestData = chrome.runtime.getManifest();
    const versionText = `Version ${manifestData.version}`;
    if ($('#appVersion')) {
      $('#appVersion').text(versionText);
    }
  } catch (e) { /* empty */ }
}
let popupImageCollapses = [];
function collapseOpenImageOnClick(event) {
  if (event.target?.id) {
    const filteredCollapses = popupImageCollapses.filter((e) => e.id !== event.target.id);
    // eslint-disable-next-line no-restricted-syntax
    for (const filtered of filteredCollapses) {
      // eslint-disable-next-line no-undef
      bootstrap.Collapse?.getInstance(filtered)?.hide();
    }
  }
}

window.onload = function () {
  $('#helpDescriptionContent').load('helpDescription.html', () => {
    accordionCollapseScroll();
    popupImageCollapses = [].slice.call(document.getElementsByClassName('collapseCollect'));
    if (popupImageCollapses.length > 0) {
      // eslint-disable-next-line no-restricted-syntax
      for (const popupImageCollapse of popupImageCollapses) {
        popupImageCollapse.addEventListener('show.bs.collapse', collapseOpenImageOnClick);
      }
    }
  });
};
