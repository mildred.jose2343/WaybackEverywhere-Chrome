/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2022 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/
import { Redirect } from '../commonUtils/redirect.js';
import { UrlHelper } from '../commonUtils/parseUrl.js';
import { getRegex } from '../commonUtils/verifyRules.js';
import { clearAllTemps, addSitetoExclude } from '../messageHandlers/index.js';

// eslint-disable-next-line no-unused-vars
let tempIncludes = [];
let commonExtensions = [];
const counts = {
  archivedPageLoadsCount: 0,
  waybackSavescount: 0,
};
/* eslint-disable no-unused-vars */
// these unused variables are from mv2 extension, clean up after mv3 version first release
let tempExcludes = [];
let isLoadAllLinksEnabled = false;
/* eslint-enable no-unused-vars */

function log(msg) {
  if (log.enabled) {
    // eslint-disable-next-line no-console
    console.log(`WaybackEverywhere: ${msg} at ${new Date()}`);
  }
}

log.enabled = false;

function updateLogging() {
  chrome.storage.local.get(
    {
      logging: false,
    },
    (obj) => {
      log.enabled = obj.logging;
      log(`logging for Wayback Everywhere toggled to..${log.enabled}`);
    },
  );
}

updateLogging();
// Redirects partitioned by request type, so we have to run through
// the minimum number of redirects for each request.
let partitionedRedirects = {};

// Cache of urls that have just been redirected to. They will not be redirected again, to
// stop recursive redirects, and endless redirect chains.
// Key is url, value is timestamp of redirect.
const ignoreNextRequest = {};

// url => { timestamp:ms, count:1...n};
const justRedirected = {};
const redirectThreshold = 3;

let filters = [];
let excludePatterns;

async function storeCountstoStorage() {
  log(`Setting counts to storage as ${JSON.stringify(counts)}`);
  await chrome.storage.local.set({
    counts,
  });
}
// not possible with mv3, mv2 code had these two setIntervals
// setInterval(storeCountstoStorage, 240000);
// 4 minutes once, write counts to disk
// setInterval(clearJustSaved, 240000);
// 4 Minutes once, clear JustSaved based on time.

let justSaved = ['http://examples.com==WBE==9999999999999'];
const justreloaded = [];

function clearJustSaved() {
  for (let j = 0; j < justSaved.length; j += 1) {
    if (Date.now() - Number(justSaved[j].split('==WBE==')[1]) >= 240000) {
      justSaved.splice(j, 1);
      // Do not 'break' here, just clear out all old links in justSaved.
    }
  }
  chrome.storage.local.set({ justSaved });
  for (let m = 0; m < justreloaded.length; m += 1) {
    if (Date.now() - Number(justreloaded[m].split('==WBE==')[1]) >= 240000) {
      justreloaded.splice(m, 1);
      // Do not 'break' here, just clear out all old links in justreloaded.
    }
  }
}

// This is the actual function that gets called for each page load in a tab url bar and must
// decide whether or not we want to redirect.
async function checkRedirects(detailsInfo) {
  const details = { ...detailsInfo };
  if (!details) {
    // This seems to happen for maybe blank page in new tab? We need url to proceed
    return;
  }

  if (!details.url || !details.tabId) {
    return;
  }

  // console.log(JSON.stringify(details))

  // just to reuse the mv2 code, attach method and type.
  if (!details.method) {
    details.method = 'GET';
  }
  if (!details.type) {
    details.type = 'main_frame';
  }
  const { tabId } = details;

  // Once wayback redirect url is loaded, we can just return it except when it's in exclude pattern.
  // this is for issue https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/7
  // When already in archived page, Wayback Machine prefixes web.archive.org/web/2/
  // to all URLs in the page
  // For example, when viewing archived site, there's a github link -
  // and if github is in Excludes list,
  // Using this, we load live page of github since it's in excludes list.
  // we may add a switch to Settings page to disable this behaviour at a later time if needed.

  // Need to use once we make Excludepattern array of hosts instead of regex
  // if(excludePatterns.indexOf(host))

  try {
    // Return save page requests right away
    if (details.url.indexOf('web.archive.org/save') > -1) {
      return;
    }

    const urlDetails = UrlHelper.getHostfromUrl(details.url);

    if (urlDetails.hostname.length === 0) {
      // UrlHelper.getHostfromUrl will return empty hostname
      // if you try to load archived version of Wayback Machine itself.
      return;
    }

    if (details.url.indexOf('web.archive.org/web') > -1) {
      // If user tries to click on login, register, or forgot password page,
      // URL might have the words which we can identify
      // and add the site to temporary excludes.
      // Temporary excludes are cleared upon browser restart.
      if (
        urlDetails.url.indexOf('/login') > -1
        || urlDetails.url.indexOf('/signin') > -1
        || urlDetails.url.indexOf('login.htm') > -1
        || urlDetails.url.indexOf('login.php') > -1
        || urlDetails.url.indexOf('signin.php') > -1
        || urlDetails.url.indexOf('signin.htm') > -1
        || urlDetails.url.indexOf('login.asp') > -1
        || urlDetails.url.indexOf('login.asp') > -1
        || urlDetails.url.indexOf('/join') > -1
        || urlDetails.url.indexOf('signup.html') > -1
        || urlDetails.url.indexOf('signup.php') > -1
        || urlDetails.url.indexOf('signup.asp') > -1
        || urlDetails.url.indexOf('/profile/create') > -1
        || urlDetails.url.indexOf('/password_reset') > -1
        || urlDetails.url.indexOf('/password') > -1
        || urlDetails.url.indexOf('/resetpassword') > -1
        || urlDetails.url.indexOf('/forgotpassword') > -1
        || urlDetails.url.indexOf('/forgot_password') > -1
        || urlDetails.url.indexOf('/recoverpassword') > -1
        || urlDetails.url.indexOf('/register') > -1
        || urlDetails.url.indexOf('register.html') > -1
        || urlDetails.url.indexOf('register.asp') > -1
        || urlDetails.url.indexOf('register.php') > -1
        || urlDetails.url.indexOf('/myaccount') > -1
        || urlDetails.url.indexOf('/sign_in') > -1
        || urlDetails.url.indexOf('/sign_up') > -1
        || urlDetails.url.indexOf('/recover_password') > -1
        || urlDetails.url.indexOf('/owa') > -1
        || urlDetails.url.indexOf('/Outlook') > -1
        || urlDetails.url.indexOf('/outlook') > -1
        || urlDetails.url.indexOf('/unsubscribe') > -1
        || urlDetails.url.indexOf('/Unsubscribe') > -1
        || urlDetails.url.indexOf('/mail') > -1
        || urlDetails.url.indexOf('/webmail') > -1
        || urlDetails.url.indexOf('/myaccount') > -1
        || urlDetails.url.indexOf('/MyAccount') > -1
        || urlDetails.url.indexOf('/account') > -1
        || urlDetails.url.indexOf('/Account') > -1
      ) {
        log(
          `Loginfound or signup found in URL. Adding site to excludes ${
            urlDetails.hostname
          } when you tried to load${
            details.url}`,
        );
        const request = {};
        request.subtype = 'fromContent';
        request.category = 'excludethisSite';
        const sender = { tab: {} };
        sender.tab = {};
        sender.tab.id = details.tabId;
        sender.tab.url = details.url;
        // eslint-disable-next-line no-unused-vars
        const sendResponse = (_obj) => { };
        addSitetoExclude(request, sender, sendResponse);
        return;
      }
    }
    const ambiguousExcludes = [
      't.co',
      'g.co',
      'ft.com',
      'cnet.co',
      'vine.co',
      'ted.com',
      'f-st.co',
    ];
    // since "t.co" shoterner matches with sites that have "..t.com" in the url as we use RegExp
    // As t.co is the most common for links clicked from tweets -
    // let's check and return t.co without further processing
    // https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/13

    // Made the below a loop now after ft.com matched against sites that have ft.com in the URL..
    // https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/22

    for (let i = 0; i < ambiguousExcludes.length; i += 1) {
      if (urlDetails.hostname === ambiguousExcludes[i]) {
        // We need this if condition to avoid infinite redirects of t.co url into itself.
        // That is, if web.archive.org is prefixed to t.co, just load t.co live url
        // so that this shortener can expand to actual URL
        // If web.archive.org is NOT prefixed, just return -
        // as it can continue to expand to live URL which will get redirected to WM later.
        if (
          details.url.replace('#close', '')
          !== urlDetails.url.replace('#close', '')
        ) {
          // eslint-disable-next-line no-await-in-loop
          const results = await chrome.tabs.update(tabId, { url: urlDetails.url });
          log(`Ambigious domain name, loading live : ${JSON.stringify(results)}`);
          return;
        }
        log(`Ambigious domain name, loading live : ${JSON.stringify(details)}`);
        return;
      }
    }

    // https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/20
    // Issue happens when a blog redirects to medium globalidentify for some reason
    // .. as we don't have medium in Excludes list -
    // since medium.com articles work fine with Wayback machine
    // .. just programatically add these redirect blogs to excludes list and load live page.
    // do this programatically instead of adding to settings.json,
    // so that user can keep building her Excludes list using this too.
    // Example :
    // https://medium.com/m/global-identity?redirectUrl=https://blog.mapbox.com/hd-vector-maps-open-standard-335a49a45210

    if (
      urlDetails.hostname === 'medium.com'
      && urlDetails.url.indexOf('global-identity?redirect') > -1
      && details.url.indexOf('web.archive.org') > -1
    ) {
      const request = {};
      request.subtype = 'fromContent';
      request.category = 'addtoExclude';
      const sender = { tab: {} };
      sender.tab = {};
      sender.tab.id = details.tabId;
      const index = urlDetails.url.indexOf('redirectUrl=') + 12;
      sender.tab.url = `https://web.archive.org/web/2/${
        decodeURIComponent(urlDetails.url.substring(index))}`;
      const sendResponse = () => { };
      addSitetoExclude(request, sender, sendResponse);
      log(`Adding this site to exclude as this is a medium redirect that does not work with Wayback Machine ${JSON.stringify(details)}`);
      return;
    }

    if (details.url.indexOf('web.archive.org/web') > -1) {
      // #23 - https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/23
      // Load live url when url ends with common file extensions
      // so that user can download a file easily example.com/path/to/dir/file.zip
      if (commonExtensions.length > 0) {
        let isDownloadlink = false;

        let liveURL = urlDetails.url.toLowerCase();
        if (liveURL.endsWith('#close')) {
          // eslint-disable-next-line prefer-destructuring
          liveURL = liveURL.split('#close')[0];
        }
        let index = liveURL.indexOf('?');
        if (index > -1) {
          liveURL = liveURL.substring(0, index);
          // index = -1;
        }
        if (liveURL.endsWith('/')) {
          liveURL = liveURL.substring(0, liveURL.length - 1);
        }
        index = liveURL.indexOf('#');
        if (index > -1) {
          liveURL = liveURL.substring(0, index);
        }

        for (let j = 0; j < commonExtensions.length; j += 1) {
          if (liveURL.endsWith(commonExtensions[j])) {
            isDownloadlink = true;
            break;
          }
        }
        if (isDownloadlink) {
          const results = await chrome.tabs.update(tabId, { url: urlDetails.url });
          log(`File download link detected based on file extension, load the live link instead as Wayback Machine may not cache files. ${urlDetails?.url} results : ${JSON.stringify(results)}`);
          return;
        }
      }

      // Issue 12   https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/12
      let isJustSaved = false;
      const toSaveurl = urlDetails.url.replace('#close', '');
      for (let k = 0; k < justSaved.length; k += 1) {
        if (justSaved[k].indexOf(toSaveurl) > -1) {
          isJustSaved = true;
          break;
        }
      }
      if (isJustSaved) {
        log('this page is in justSaved, so loading archived version');
        return;
      }
      log(
        `Checking if this is in Excludes so that we can return live page url ..  ${
          urlDetails.url}`,
      );
      if (excludePatterns) {
        const shouldExclude = excludePatterns.test(urlDetails.hostname);
        excludePatterns.lastIndex = 0;
        if (tempIncludes.length === 0) {
          if (shouldExclude) {
            const results = await chrome.tabs.update(tabId, { url: urlDetails.url });
            log(`This page is excluded : ${JSON.stringify(results)}`);
            return; // { redirectUrl: urlDetails.url };
          }
          return;
        }
        if (tempIncludes.indexOf(urlDetails.hostname) > -1) {
          return;
        }
        if (shouldExclude) {
          const results = await chrome.tabs.update(tabId, { url: urlDetails.url });
          log(`This page is excluded : ${JSON.stringify(results)}`);
          return;
        }
      }
      return;
    }

    log(` Checking: ${details.type}: ${details.url}`);

    const list = partitionedRedirects[details.type];
    // log(list);
    if (!list) {
      log(`No list for type: ${details.type}`);
      return;
    }

    const timestamp = ignoreNextRequest[details.url];
    if (timestamp) {
      log(
        ` Ignoring ${
          details.url
        }, was just redirected ${
          new Date().getTime() - timestamp
        }ms ago`,
      );
      delete ignoreNextRequest[details.url];
      return;
    }

    for (let i = 0; i < list.length; i += 1) {
      const r = list[i];
      // if user clicks on search result link :
      if (details.url.startsWith('https://www.google.com/url?q=') || details.url.startsWith('http://www.google.com/url?q=')) {
        const split = details.url.split('google.com/url?q=');
        if (split.length > 1) {
          // eslint-disable-next-line prefer-destructuring
          details.url = split[1];
        }
      }
      log(`calling getMatch with ..${details.url}`);
      const result = r.getMatch(details.url);
      /* wmAvailabilityCheck( details.url,function onSuccess(wayback_url,url){
        log('wayback wmAvailabilityCheck passed ->  wayback_url = ' + wayback_url + ' url ' + url)
      },function onfail(){log(' wayback wmAvailabilityCheck failed')}); */

      log(`getMatch result is.. result.isMatch -> ${result.isMatch}`);
      if (result.isMatch) {
        // Check if we're stuck in a loop where we keep redirecting this, in that
        // case ignore!
        log(' checking if we have just redirected to avoid loop');
        const data = justRedirected[details.url];

        const threshold = 3000;
        if (!data || new Date().getTime() - data.timestamp > threshold) {
          // Obsolete after 3 seconds
          justRedirected[details.url] = {
            timestamp: new Date().getTime(),
            count: 1,
          };
        } else {
          data.count += 1;
          justRedirected[details.url] = data;
          if (data.count >= redirectThreshold) {
            log(
              ` Ignoring ${
                details.url
              } because we have redirected it ${
                data.count
              } times in the last ${
                threshold
              }ms`,
            );
            return;
          }
        }

        log(
          ` Redirecting ${
            details.url
          } ===> ${
            result.redirectTo
          }, type: ${
            details.type
          }, pattern: ${
            r.includePattern}`,
        );

        ignoreNextRequest[result.redirectTo] = new Date().getTime();

        counts.archivedPageLoadsCount += 1;
        // console.log(JSON.stringify(ignoreNextRequest))
        // console.log(counts.archivedPageLoadsCount);

        log(`counts updated to ${counts.archivedPageLoadsCount}`);
        // Issue 10 - https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox/issues/10
        // Remove ?utm and others and redirect only direct clean URL to wayback machine
        result.redirectTo = UrlHelper.cleanUrlsOnFilters(result.redirectTo, filters);
        log(`Redirecting to ......${result.redirectTo}`);
        // eslint-disable-next-line no-await-in-loop
        const results = await chrome.tabs.update(tabId, { url: result.redirectTo });
        log(`Results ${JSON.stringify(results)}`);
        return;
        // return {
        //   redirectUrl: result.redirectTo
        // }; //mv2
      }
      log(`No rules apply, no action taken: ${JSON.stringify(details)}`);
    }
  } catch (e) {
    log(`Error ${e}`);
  }
}

// Monitor changes in data, and setup everything again.
// This could probably be optimized to not do everything on every change
// but why bother
// eslint-disable-next-line no-unused-vars
function monitorChanges(changes, _namespace) {
  if (changes.redirects) {
    const newRedirects = changes.redirects.newValue;
    excludePatterns = getRegex(newRedirects[0].excludePattern);
  }

  if (changes.logging) {
    updateLogging();
  }
  if (changes.tempIncludes) {
    log(`tempIncludes changed to${changes.tempIncludes.newValue}`);
    tempIncludes = changes.tempIncludes.newValue;
  }
  if (changes.tempExcludes) {
    log(`tempExcludes changed to${changes.tempExcludes.newValue}`);
    tempExcludes = changes.tempExcludes.newValue;
  }

  if (changes.filters) {
    filters = changes.filters.newValue;
  }

  if (changes.isLoadAllLinksEnabled) {
    isLoadAllLinksEnabled = changes.isLoadAllLinksEnabled.newValue;
  }

  if (changes.counts) {
    // This is needed only when user resets stats to 0 from advanceduser page
    if (
      changes.counts.newValue.archivedPageLoadsCount === 0
      && changes.counts.newValue.waybackSavescount === 0
    ) {
      counts.archivedPageLoadsCount = 0;
      counts.waybackSavescount = 0;
    }
  }
  if (changes.commonExtensions) {
    commonExtensions = changes.commonExtensions.newValue;
  }
}

// TODO: move Remove from Excludes from popup.js to here
// i.e Temporary incldue or Include should go here, currently it's in popup.js

function createPartitionedRedirects(redirects) {
  const partitioned = {};

  for (let i = 0; i < redirects.length; i += 1) {
    const redirect = new Redirect(redirects[i]);
    redirect.compile();
    for (let j = 0; j < redirect.appliesTo.length; j += 1) {
      const requestType = redirect.appliesTo[j];
      if (partitioned[requestType]) {
        partitioned[requestType].push(redirect);
      } else {
        partitioned[requestType] = [redirect];
      }
    }
  }

  return partitioned;
}

// Sets up the listener, partitions the redirects, creates the appropriate filters etc.
async function onBeforeNavigate(details) {
  log(' onBeforeNavigate  starts.. Check if temporarily disabled, otherwise proceed to evaluate the rules');

  if (!details) {
    return;
  }
  if (details.frameId !== 0 || !details.url) {
    return;
  }
  const { disabled } = await chrome.storage.session.get({
    disabled: false,
  });
  if (disabled) {
    log(' Wayback Everywhere is currently disabled');
    return;
  }
  const obj = await chrome.storage.local.get(
    {
      redirects: [],
      commonExtensions: [],
      filters: [],
      tempExcludes: [],
      tempIncludes: [],
      counts: {},
      justSaved: [],
    },
  );

 
  justSaved = obj.justSaved;
  counts.archivedPageLoadsCount = obj.counts?.archivedPageLoadsCount ?? 0;
  counts.waybackSavescount = obj.counts?.waybackSavescount ?? 0;
  tempExcludes = obj.tempExcludes;
  tempIncludes = obj.tempIncludes;
  commonExtensions = obj.commonExtensions;
  filters = obj.filters;
  const { redirects } = obj;
  if (redirects.length === 0) {
    log(' No redirects defined, not setting up listener');
    return;
  }
  excludePatterns = getRegex(redirects[0].excludePattern);
  // (we need to make ExcludePattern an array of hosts, currently it's regex)

  partitionedRedirects = createPartitionedRedirects(redirects);
  clearJustSaved();
  checkRedirects(details);
  await storeCountstoStorage();

  //  var filter = createFilter(redirects);
  // chrome.webRequest.onBeforeRequest.addListener(checkRedirects, filter, [
  //   "blocking"
  // ]);
}

// this won't work in mv3. Mv2 specific code, find alternative. TODO

// Added the below to hande a very rare case where Wayback throws "504" error when Saving page.
// Manually reloading the page was enough to get it work next time.
// This will just reload the page once and stop reloading after that if it continues as
// .. it assigned url to justreloaded variable

// Find out if 504 is thrown by the saved page or by WM itself -
// Need to Comment out the below if WM is actually the one that shows this 504

// function reloadPage(tabId, tabUrl) {
//   let shouldReload = false;
//   for (let i = 0; i < justreloaded.length; i++) {
//     if (justreloaded[i].indexOf(tabUrl) < 0) {
//       shouldReload = true;
//       justreloaded.push(tabUrl + "==WBE==" + Date.now());
//       break;
//     }
//   }
//   if (shouldReload) {
//     chrome.tabs.reload(
//       tabId,
//       {
//         bypassCache: true
//       },
//       function () {
//         log("Page reloaded");
//       }
//     );
//   }
// }
// chrome.webRequest.onCompleted.addListener(
//   function(details) {
//     /*if (details.type == "main_frame") {
//     console.log("status code is " + details.statusCode + " in url " + details.url);
//   } */
//     if (
//       details.type == "main_frame" &&
//       (details.statusCode == 504 || details.statusCode == 503)
//     ) {
//       reloadPage(details.tabId, details.url);
//     }
//   },
//   {
//     urls: ["*://web.archive.org/*"]
//   }
// );

// First time setup
// updateIcon();

chrome.storage.session.get(
  {
    disabled: false,
  },
  (obj) => {
    if (!obj.disabled) {
      // onBeforeNavigate();
      log('Wayback Everywhere is enabled');
    } else {
      log(`Wayback Everywhere is disabled...${obj.disabled}`);
    }
  },
);

log(' Wayback Everywhere starting up...');

// chrome.tabs.onUpdated.addListener(tabUpdatedListener);//TODO

function handleStartup() {
  log(
    'Handle startup - fetch counts, fetch readermode setting, fetch appdisabled setting, clear out any temp excludes or temp includes',
  );
  // For issue 11 fix, we moved this to background script -
  // so that counts can get set to global variables correctly..
  // .. when addon is disabled and enabled from about:addons . Commenting the below
  /*
   chrome.storage.local.get({
    counts: counts
  }, function(response) {
    counts.archivedPageLoadsCount = response.counts.archivedPageLoadsCount;
    counts.waybackSavescount = response.counts.waybackSavescount;
    oldcounts = JSON.parse(JSON.stringify(counts));
  });  */
  chrome.storage.local.get(
    {
      isLoadAllLinksEnabled: true,
    },
    (obj) => {
      isLoadAllLinksEnabled = obj.isLoadAllLinksEnabled;
    },
  );

  chrome.storage.local.get(
    {
      operationMode: false,
    },
    async (obj) => {
      // operationMode -> false is default behaviour of turning on WBE when browser loads.
      // true - if user wishes to start browser with WBE disabled
      // eslint-disable-next-line no-console
      console.log(`Wayback Everywhere - Operation mode: ${obj.operationMode} If true, opted to start browser with Wayback disabled. Can be temporarily enabled from popup menu`);
      await chrome.storage.session.set({
        disabled: obj.operationMode,
      });
    },
  );
  /*
    // Enable on startup - Popup button is "Temporarily disable.."
    // as user can do full disable from addon/extension page anyway
    chrome.storage.local.set({
      disabled: false
    }); */
  // Disable logging on startup
  // disabled in local storage was in mv2 version, make it false as it should be unused now in mv3
  chrome.storage.local.set({
    logging: false,
    disabled: false
  });
  // eslint-disable-next-line no-unused-vars
  const sendResponse = (_obj) => { };
  clearAllTemps(sendResponse);
  chrome.storage.local.set({ justSaved: [] });
}

export { handleStartup, monitorChanges, onBeforeNavigate };
