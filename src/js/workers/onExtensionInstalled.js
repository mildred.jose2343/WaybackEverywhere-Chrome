/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2022 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { loadInitialdata } from '../appConfig/initialSettings.js';
import { handleUpdate } from './onExtensionUpdated.js';

function onInstalled(details) {
  // console.log(JSON.stringify(details));
  if (details.reason === 'install') {
    loadInitialdata('init');
    // eslint-disable-next-line no-console
    console.log(' Wayback Everywhere addon installed');

    const counts = {
      archivedPageLoadsCount: 0,
      waybackSavescount: 0,
    };
    const STORAGE = chrome.storage.local;
    STORAGE.set({
      counts,
    });

    const tempExcludes = [];
    STORAGE.set({
      tempExcludes,
    });
    STORAGE.set({
      tempIncludes: tempExcludes,
    });
  }

  if (details.reason === 'update') {
    handleUpdate(details.temporary);
    // To add or remove from default excludes and others settings using appconfig/updates.js
    // eslint-disable-next-line no-console
    console.log(' Wayback Everywhere addon was updated');
  }

  if (details.reason === 'install' && details.temporary !== true) {
    const url = chrome.runtime.getURL('help.html');
    // eslint-disable-next-line no-console
    console.log('Wayback Everywhere addon installed or updated..');
    chrome.tabs.create({
      url,
    });
  }
}

export { onInstalled };
