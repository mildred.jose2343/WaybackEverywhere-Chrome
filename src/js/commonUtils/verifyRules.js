/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2022 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

function getRegex(excludePatterns) {
  let converted = '^';
  for (let i = 0; i < excludePatterns.length; i += 1) {
    const ch = excludePatterns.charAt(i);
    if ('()[]{}?.^$\\+'.indexOf(ch) !== -1) {
      converted += `\\${ch}`;
    } else if (ch === '*') {
      converted += '(.*?)';
    } else {
      converted += ch;
    }
  }
  converted += '$';
  return new RegExp(converted, 'gi');
}

const checkifDomaininAnyList = (domain, tempIncludes, tempExcludes, excludePatterns) => {
  let domainStatus = 'notInAnyList';
  if (!!domain && domain.length > 0) {
    // let pattern = '|*' + domain + '*';
    let tempExcRegex; let
      tempIncRegex;
    if (!!tempExcludes && tempExcludes.length > 0) {
      tempExcRegex = getRegex(tempExcludes.join('').substring(1));
    }
    if (!!tempIncludes && tempIncludes.length > 0) {
      tempIncRegex = getRegex(tempIncludes.join('').substring(1));
    }
    const excListRegex = excludePatterns;
    if (!!tempExcRegex && tempExcRegex.test(domain)) {
      domainStatus = 'domainTempExcluded';
      tempExcRegex.lastIndex = 0;
    } else if (!!tempIncRegex && tempIncRegex.test(domain)) {
      domainStatus = 'domainTempIncluded';
      tempIncRegex.lastIndex = 0;
    } else if (!!excListRegex && excListRegex.test(domain)) {
      excListRegex.lastIndex = 0;
      domainStatus = 'domainInExcludes';
    }
  }
  return domainStatus;
};

export { getRegex, checkifDomaininAnyList };
