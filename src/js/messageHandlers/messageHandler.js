/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2022 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

import { log } from "../devTools/log.js";
import { loadInitialdata } from "../appConfig/initialSettings.js";
import {
  checkifDomaininAnyList,
  getRegex,
} from "../commonUtils/verifyRules.js";
import { UrlHelper } from "../commonUtils/parseUrl.js";
import {
  savetoWM,
  clearAllTemps,
  clearTemps,
  addSitetoExclude,
} from "./index.js";

function MessageHandler(request, sender, sendResponse) {
  const manifestData = chrome.runtime.getManifest();

  log(` Received background message: ${JSON.stringify(request)}`);
  if (request.type === "getredirects") {
    log("Getting redirects from storage");
    chrome.storage.local.get(
      {
        redirects: [],
      },
      (obj) => {
        if (obj.redirects.length > 0) {
          log(`Got redirects from storage: ${obj.redirects[0].description}`);
        }
        sendResponse(obj);
      }
    );
  } else if (request.type === "saveredirects") {
    delete request.type;
    // eslint-disable-next-line no-unused-vars
    chrome.storage.local.set(request, (_a) => {
      log("Finished saving redirects to storage from url");
      sendResponse({
        message: "Redirects saved",
      });
    });
  } else if (request.type === "excludethisSite") {
    delete request.type;
    chrome.storage.session.get(
      {
        disabled: false,
      },
      ({ disabled }) => {
        if (!disabled) {
          addSitetoExclude(request, sender, sendResponse);
        }
      }
    );
  } else if (request.type === "notify") {
    delete request.type;
    chrome.notifications.create({
      type: "basic",
      title: "Wayback Everywhere",
      message: request.data,
      iconUrl: "/images/icon@2x.png",
    });
  } else if (request.type === "doFullReset") {
    const resettype = request.type;
    delete request.type;
    // FileReader.loadInitialdata(() => {
    //   log('finished full  reset, returning response to setting page');
    //   sendResponse({
    //     message: ' Factory reset. Reloaded  settings from bundled json'
    //   });
    // });
    loadInitialdata(resettype);
  } else if (request.type === "savetoWM") {
    delete request.type;
    chrome.storage.session.get(
      {
        disabled: false,
      },
      ({ disabled }) => {
        if (!disabled) {
          savetoWM(request, sender, sendResponse);
        }
      }
    );
  } else if (request.type === "appDetails") {
    let domainStatus = "notInAnyList";

    chrome.storage.local.get(
      {
        counts: {},
        tempExcludes: [],
        tempIncludes: [],
        isLoadAllLinksEnabled: false,
        filters: [],
        commonExtensions: [],
        redirects: [],
        justSaved: [],
        logging: false,
      },
      (result) => {
        const { tempExcludes } = result;
        const { tempIncludes } = result;
        const { filters } = result;
        const excludePatterns = getRegex(result.redirects[0].excludePattern);
        if (request.subtype === "fromPopup") {
          // eslint-disable-next-line max-len
          domainStatus = checkifDomaininAnyList(
            request.domain,
            tempIncludes,
            tempExcludes,
            excludePatterns
          );
        }
        chrome.storage.session.get(
          {
            disabled: false,
          },
          ({ disabled }) => {
            const c = {
              logstatus: result.logging,
              counts: JSON.stringify(result.counts),
              appDisabled: disabled,
              tempExcludes,
              tempIncludes,
              // excludesList: excludesList,
              domainStatus,
              isLoadAllLinksEnabled: result.isLoadAllLinksEnabled,
              justSaved: result.justSaved,
              filters,
              appVersion: manifestData.version,
              commonExtensions: result.commonExtensions,
            };
            // log("App Details at " + new Date() + " : " + JSON.stringify(c))
            sendResponse(c);
          }
        );
      }
    );
  } else if (request.type === "openAllLinks") {
    log(JSON.stringify(request));
    delete request.type;
    const urls = request.data;

    for (let i = 0; i < urls.length; i += 1) {
      if (
        request.selector.length !== 0 &&
        urls[i].indexOf(request.selector) > -1
      ) {
        if (urls[i].indexOf("http") !== 0) {
          if (urls[i].indexOf("/web") === 0) {
            urls[i] = `https://web.archive.org${urls[i]}`;
            // console.log(urls[i]);
          }
        }
        log(`Opening this url in new tab -> ${urls[i]}`);

        chrome.tabs.create({
          url: urls[i],
        });
      }
    }
  } else if (request.type === "seeFirstVersion") {
    delete request.type;
    const urlDetails = UrlHelper.getHostfromUrl(request.url);
    const firstVersionURL = `https://web.archive.org/web/0/${urlDetails.url}`;
    chrome.tabs.update(
      request.tabid,
      {
        active: true,
        url: firstVersionURL,
      },
      // eslint-disable-next-line no-unused-vars
      (_tab) => {
        log(
          `first version url loaded in browser in tab ${request.tabid} as ${firstVersionURL}`
        );
        sendResponse("loaded first archived version");
      }
    );
  } else if (request.type === "clearTemps") {
    delete request.type;
    if (request.subtype === "fromPopup") {
      clearTemps(request.domain, request.toClear, sendResponse);
    } else {
      clearAllTemps(sendResponse);
    }
  } else if (request.type === "impatientLink") {
    delete request.type;
    if (request.url) {
      chrome.storage.session.get(
        {
          disabled: false,
        },
        ({ disabled }) => {
          if (!disabled) {
            const toLoadUrl = request.url;
            const tabid = sender.tab.id;
            chrome.tabs.update(tabid, {
              url: toLoadUrl,
            });
          }
        }
      );
    }
    sendResponse(`loaded ${request.url}`);
  } else if (request.type === "executeOpenAllLinks") {
    delete request.type;
    chrome.tabs.sendMessage(
      request.tabid,
      {
        type: "getAllFirstPartylinks",
        selector: request.selector,
      },
      // eslint-disable-next-line no-unused-vars
      (_response) => {
        // console.log(JSON.stringify(response.data));
      }
    );
  } else {
    log(`Unexpected message: ${JSON.stringify(request)}`);
    return false;
  }
  return true; // we're sending the response asynchronously.
}

export { MessageHandler };
