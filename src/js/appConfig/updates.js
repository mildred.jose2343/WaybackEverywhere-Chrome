/** *****************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2022 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Chrome
*/

const updates = {
  changeInAddList: true,
  changeInRemoveList: false,
  changeInAddtoFiltersList: false,
  changeInRemovefromFiltersList: false,
  addToDefaultExcludes: "|*paypal.*",
  removeFromDefaultExcludes: [],
  // These Filters apply only for URLs that are loaded from Wayback Machine
  // These do not apply for excluded sites.
  // For excluded sites, there are other extensions for e.g.
  // For e.g. 'ClearURLs' does clear up of these query parameters based on filters mapped to sites
  addtoFiltersList: [],
  removefromFiltersList: [],
  showUpdatehtml: true,
  changeinAddtoCommonFileExtensions: false,
  changeinRemovefromCommonFileExtensions: false,
  addtoCommonFileExtensions: [],
  removefromCommonFileExtensions: [],
};

export { updates };
